package animal;

public class Animal{
    protected String nama;
    protected short bodyLength;
    protected String jenisHewan;
    
    //KONSTRUKTOR
    public Animal(String nama, short bodyLength, String jenisHewan) {
        this.nama = nama;
        this.bodyLength = bodyLength;
        this.jenisHewan = jenisHewan;
    }
    
    public String getNama(){
        return this.nama;
    }
    
    public short bodyLength(){
        return this.bodyLength;
    }
    
    public String getJenisHewan(){
        return this.jenisHewan;
    }
}