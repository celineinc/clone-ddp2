package animal;

import java.util.*;

public class Hamsters extends Animal{

    public Hamsters(String namaHamsters, short panjangTubuh){
        super(namaHamsters, panjangTubuh, "hamster");
    }
    
    public void actions(){
        
        System.out.println("1: See it gnawing 2: Order to run in hamster wheel");
        Scanner action = new Scanner(System.in);
        int option = action.nextInt();
        
        if (option == 1){
            System.out.println(nama + " makes a voice: Ngkkrit.. Ngkkrrriiit \n" +
                              "Back to the office!");
        } else if(option == 2) {            
            System.out.println(nama + " makes a voice: Trrr... Trrr... \n" +
                              "Back to the office!");
        } else{
            System.out.println("You do nothing...\n" +
            "Back to the office!");
        }
    }
}