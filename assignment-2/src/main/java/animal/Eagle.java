package animal;

import java.util.*;

public class Eagle extends Animal{
    
    public Eagle(String namaEagle, short panjangTubuh){
        super(namaEagle, panjangTubuh, "eagle");
    }
    
    public void actions(){
        
        System.out.println("1: Order to fly");
        Scanner action = new Scanner(System.in);
        int option = action.nextInt();
        
        if (option == 1){
            System.out.println(nama + " makes a voice: Kwaakk...." +
                             "\n You hurt!" +
                             "\n Back to the office!");
        } else{
            System.out.println("You do nothing...\n" +
            "Back to the office!");
        }
    }
}