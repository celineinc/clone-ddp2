package animal;

import java.util.*;

public class Lion extends Animal{
    
    public Lion(String namaSinga, short panjangTubuh){
        super(namaSinga, panjangTubuh, "lion");
    }
    
    public void actions(){
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
        Scanner action = new Scanner(System.in);
        int option = action.nextInt();
        
        if (option == 1){
            System.out.println("Lion is hunting... \n" +
                             nama + " makes a voice: Err.... \n" +
                              "Back to the office!");
        } else if(option == 2){
            System.out.println("Clean the lion's mane.. \n" +
                                nama + " makes a voice: Hauhhmm! \n" + 
                                "\n Back to the office!");
        } else if(option == 3){
            System.out.println(nama + " makes a voice: HAAHUM!! \n" + 
                                "\n Back to the office!");
        } else {
            System.out.println("You do nothing...\n" +
            "Back to the office!");
        }
    }
}