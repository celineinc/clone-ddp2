package animal;

import java.util.*;

public class Cat extends Animal{
    
    private static final String[] suara = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
 
    public Cat(String namaKucing, short panjangTubuh){
        super(namaKucing, panjangTubuh, "cat");
    }
    
    public void actions(){
        Random random = new Random();
        
        System.out.println("1: Brush the fur 2: Cuddle");
        Scanner action = new Scanner(System.in);
        int option = action.nextInt();
        
        if (option == 1){
            System.out.println("Time to clean " + nama + "'s fur \n" +
                           nama + " makes a voice: Nyaaan..." + "\n" +
                              "Back to the office!");
        } else if(option == 2) {
            System.out.println(nama + " makes a voice: " + suara[random.nextInt(4)] + "\n" +
                              "Back to the office!");
        } else{
            System.out.println("You do nothing...\n" +
            "Back to the office!");
        }
    }
}