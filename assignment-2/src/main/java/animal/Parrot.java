package animal;

import java.util.*;

public class Parrot extends Animal{
    
    public Parrot(String namaBeo, short panjangTubuh){
        super(namaBeo, panjangTubuh, "parrot");
    }
    
    public void actions(){
        
        System.out.println("1: Order to fly 2: Do conversation");
        Scanner action = new Scanner(System.in);
        int option = action.nextInt();
        
        if (option == 1){
            System.out.println("Parrot " + nama + " flies \n" +
                           nama + " makes a voice: TERBAAANG..." + "\n" +
                              "Back to the office!");
        } else if(option == 2) {
            System.out.print("You say: ");
            Scanner kata = new Scanner(System.in);
            System.out.println(nama + " says: " + kata.nextLine().toUpperCase() + "\n" +
                              "Back to the office!");
        } else{
            System.out.println(nama + " says: HM? \n" +
            "Back to the office!");
        }
    }
}