package cage;

import animal.*;
import java.util.*;

public class Cage{
    
    private Animal binatang;
    private char kandang;
    
    public Cage(Animal binatang, String location){
        this.binatang = binatang;
        
        if(location.equals("indoor")) {
            if(binatang.getLength() <= 60) {
                this.kandang = "B";
            } else if(binatang.getLength < 45) {
                this.kandang = "A";
            } else{
                this.kandang = "C";
            }
        } else if(location.equals("outdoor")) {
            if(binatang.getLength() <= 90) {
                this.kandang = "B";
            } else if(binatang.getLength < 75) {
                this.kandang = "A";
            } else{
                this.kandang = "C";
            }
        }   
    }
    
    public String toString() {
        return " " + this.binatang.getName() + " (" + this.binatang.getLength() + " - " + this.kandang + "),";
    }
}