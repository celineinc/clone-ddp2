package cage;

import animal.*;
import java.util.*;

public class Cages{
    
    private String location;
    private String jenisHewan;
    private ArrayList<Animal> binatang;
    private ArrayList<Cage>[] kandang = new ArrayList[3];
    private static ArrayList<Cages> kumpulanKandang = new ArrayList<Cages>();
    
    public Cages(ArrayList<Animal> binatang, String location) {
        this.binatang = binatang;
        this.location = location;
        this.jenisHewan = binatang.get(0).getJenisHewan();
    }
 
    public String getLocation(){
        return this.location;
    }
    
    public String getJenisHewan() {
        return this.jenisHewan;
    }
    
    public ArrayList<Animal> getBinatang(){
        return this.binatang;
    }
    
    public static void aturKandang() {
        Map<String, Integer> dataHewan = new LinkedHashMap<String, Integer>();
        dataHewan.put("cat", 0);
        dataHewan.put("lion", 0);
        dataHewan.put("eagle", 0);
        dataHewan.put("parrot", 0);
        dataHewan.put("hamster", 0);

        System.out.println("=============================================\n"
                            + "Cage arrangement:");
                            
        for (Cages currentCages : kumpulanKandang) {

            dataHewan.put(currentCages.getJenisHewan(), currentCages.getBinatang().size());

            System.out.println("location: " + currentCages.getLocation());

            currentCages.getBinatangInfo();

            int j = 0;
            for (ArrayList<Cage> oldLevel : currentCages.kandang) {

                ArrayList<Cage> newLevel = new ArrayList<Cage>();

                for (int m = oldLevel.size() - 1; m >= 0; m--) {
                    newLevel.add(oldLevel.get(m));
                }

                currentCages.kandang[j++] = newLevel;
            }

            ArrayList<Cage> tem1 = currentCages.kandang[2], tem2;
            for (int l = 0; l < 3; l++) {
                tem2 = currentCages.kandang[l];
                currentCages.kandang[l] = tem1;
                tem1 = tem2;
            }

            System.out.println("After rearrangement...");

            currentCages.getBinatangInfo();
        }

        System.out.println("NUMBERS OF ANIMALS:");
        for (String key : dataHewan.keySet()) {
            System.out.println(key + ":" + dataHewan.get(key));
        }
        System.out.println("\n=============================================");

    }
    
    public void getBinatangInfo() {
        for (int i = 2; i >= 0; i--) {
            System.out.print("level " + (i + 1) + ":");

            if (!this.kandang[i].isEmpty()) {
                for (Cage kandang1 : this.kandang[i])
                    System.out.print(kandang1);
            }
            System.out.println();
        }
        System.out.println();
    }
    
}