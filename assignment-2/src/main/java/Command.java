import java.util.*;
import animal.*;
import cage.*;

public class Command{

    public static Scanner input = new Scanner(System.in);
    public static ArrayList<Animal> cats = new ArrayList<Animal>();
    public static ArrayList<Animal> lions = new ArrayList<Animal>();
    public static ArrayList<Animal> hamsters = new ArrayList<Animal>();
    public static ArrayList<Animal> eagles = new ArrayList<Animal>();
    public static ArrayList<Animal> parrots = new ArrayList<Animal>();
    
    public static Cages kandangKucing, kandangSinga, kandangHamster, kandangElang, kandangBeo;
    public Map<String, ArrayList<Animal>> kumpulanHewan = makeMapOfAnimals();
    
    public static void main(String[] args){
        
        String[] listBinatang = new String[]{"cat", "eagle", "hamster", "parrot", "lion"};
        Class[] classBinatang = new Class[]{Cat.class, Lion.class, Hamster.class, Eagle.class, Parrot.class};
        
        System.out.println("Welcome to Javari Park! \n" +
                            "Input the number of animals");
                            
        for(int i = 0; i < listBinatang.length; i++) {
            System.out.print(listBinatang[i] + ": ");
            int jumlah = input.nextInt();
            if(jumlah > 0){
                System.out.print("Provide the information of " + listBinatang[i] + "(s):");
                String data = input.nextLine();
                String[] str = data.split(",");
                    for(int j = 0; i < str.length; j++) {
                        String[] dataHewan = str[j].split("\\|");
                        String namaHewan = dataHewan[j * 2];
                        short panjangHewan = Short.parseShort(dataHewan[j * 2 + 1]);
                        //Menyimpan data Hewan di array dengan mengakses melalui key di map
                        kumpulanHewan.get(listBinatang[i]).add(createAnimal(namaHewan, panjangHewan, listBinatang[i]));
                    }
            }
        }
        System.out.println("Animals have been successfully recorded!");
        
        putInCage();
        
        while (true) {
            System.out.println("============================================= \n" +
                                "Which animal you
                                 want to visit? \n" + 
                                "1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: exit");
            int pilihHewan = input.nextInt();
            
            if (pilihHewan == 99){
                System.out.println("You don't make a visit.");
                break;
            } else if(pilihHewan > 5){
                System.out.println("There's no animal.");
                continue;
            }
            
            System.out.print("Mention the name of " + listBinatang[pilihHewan - 1]
                             + " you want to visit: " );
            String namaHewan = input.nextLine();
            
            boolean cekNamaHewan = false;
            Animal hewan = null;
            
            for (Animal  jenisBinatang : kumpulanHewan.get(listBinatang[i])) {
                if (jenisBinatang.getName().equals(namaHewan)) {
                    cekNamaHewan = true;
                    hewan = jenisBinatang;
                    break;
                }
            }
            
            if (cekNamaHewan){
                System.out.print("You are visiting " + namaHewan + 
                                "(" + listBinatang[pilihHewan - 1] + ") now, what would you like to do?");
                if(pilihHewan == 1){
                    Cat.class.cast(hewan).action();
                } else if(pilihHewan == 2){
                    Eagle.class.cast(hewan).action();
                } else if(pilihHewan == 3){
                    Hamster.class.cast(hewan).action();
                } else if(pilihHewan == 4){
                    Parrot.class.cast(hewan).action();
                }
                else if(pilihHewan == 5){
                    Lion.class.cast(hewan).action();
                }
        //Animalapakek.actions();
            }
        }
    }
    
    //METHOD UNTUK MEMBUAT MASING - MASING KELAS HEWAN
    public static void createAnimal(String namaHewan, short panjangHewan, String jenisHewan) {
        if(jenisHewan.equals("cat")) {
            return new Cat(namaHewan, panjangHewan);
        } if(jenisHewan.equals("lion")) {
            return new Lion(namaHewan, panjangHewan);
        } if(jenisHewan.equals("eagle")) {
            return new Eagle(namaHewan, panjangHewan);
        } if(jenisHewan.equals("hamster")) {
            return new Hamster(namaHewan, panjangHewan);
        } if(jenisHewan.equals("parrot")) {
            return new Parrot(namaHewan, panjangHewan);
        }
        return null;
    }
    
    public static void putInCage() {
        // Jika ada binatang, maka dibuatkan kandangnya
        if (!(cats.isEmpty())) {
            kandangKucing = new Cages(cats, "indoor");
        } if (!(lions.isEmpty())) {
            kandangSinga = new Cages(lions, "outdoor");
        } if (!(hamsters.isEmpty())) {
            kandangHamster = new Cages(hamsters, "indoor");
        } if (!(eagles.isEmpty())) {
            kandangElang = new Cages(eagles, "outdoor");
        } if (!(parrots.isEmpty())) {
            kandangBeo = new Cages(parrots, "indoor");
        }
        
        Cages.aturKandang();
    }
    
    //METHOD UNTUK MENYIMPAN KELAS ANIMAL DAN KEY NYA
    public static Map<String, ArrayList<Animal>> makeMapOfAnimals() {
        Map<String, ArrayList<Animal>> mapOfAnimals = new HashMap<String, ArrayList<Animal>>();
        mapOfAnimals.put("cat", cats);
        mapOfAnimals.put("lion", lions);
        mapOfAnimals.put("hamster", hamsters);
        mapOfAnimals.put("eagle", eagles);
        mapOfAnimals.put("parrot", parrots);
        
        return mapOfAnimals;
    }
    
    
}