package javari.animal;

public class Mammal extends Animal {
	private String kondisi;
	public static String[] jenisHewan = {"Hamster", "Lion", "Cat", "Whale"};

	//public enum jenisHewan{HAMSTER, LION, CAT, WHALE}

	public Mammal((Integer id, String type, String name, Gender gender, double length,
                  double weight, String kondisi, Condition condition) {
		super(id, type, name, gender, Length, weight, condition);
		this.kondisi = kondisi;
	}

	public boolean specificCondition() {
		if(this.kondisi.equals("pregnant")) {
			return false
		} else {
			if(this.getType().equals("Lion")) {
				if(this.body.getGender().equals(Gender.MALE)) {
					return true;
				}
			} else {
				return true;
			}
		}
	}


}