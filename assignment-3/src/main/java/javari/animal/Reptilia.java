package javari.animal;

public class Reptilia extends Animal {
	private String kondisi;
	public static String[] jenisHewan = {"Snake"};

	public Reptilia(Integer id, String type, String name, Gender gender, double length,
                  double weight, String kondisi, Condition condition) {
		super(id, type, name, gender, Length, weight, condition);
		this.kondisi = kondisi;
	}

	public boolean specificCondition() {
		if(this.kondisi.equals("tame")) {
			return true;
		}
		return false;
	}
}