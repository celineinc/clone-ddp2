package javari.animal;

public class Aves extends Animal {
	private String kondisi;
	public static String[] jenisHewan = {"Eagle", "Parrot"};

	public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition) {
		super(id, type, name, gender, Length, weight, condition);
		this.kondisi = kondisi;
	}

	public boolean specificCondition() {
		if(this.kondisi.equals("laying eggs")) {
			return false;
		}
		return true;
	}
}