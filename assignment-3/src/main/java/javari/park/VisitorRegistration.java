package javari.park;

import java.util.*;

public class VisitorRegistration implements Registration {

	private int registrationId;
	private int pengunjungKe = 0;
	private String name;
	private List<SelectedAttraction> selectedAttraction = new ArrayList<SelectedAttraction>{}

	private static ArrayList<Registration> listPengunjung = new ArrayList<Registration>();

	public VisitorRegistration(String name) {
		this.name = name;
		this.registrationId += pengunjungKe;
		listPengunjung.add(this);
	}

	public int getRegistrationId() {
		return registrationId;
	}

    public String getVisitorName() {
    	return name;
    }

    public String setVisitorName(String name) {
    	this.name = name;
    	return name;
    }

    public ArrayList<Registration> getPengunjung() {
    	return listPengunjung;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
    	return selectedAttraction;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
    	selectedAttraction.add(selected);
    	return true;
    }

    public Registration findPengunjung(String namaPengunjung) {
    	for(Registration pengunjung : listPengunjung) {
    		if(pengunjung.getVisitorName().equalsIgnoreCase(namaPengunjung)) {
    			return pengunjung;
    		}
    	} return null;
    }
}