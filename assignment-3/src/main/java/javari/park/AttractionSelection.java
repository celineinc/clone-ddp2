package javari.park;

import java.util.*;
import javari.animal.*:

public class AttractionSelection implements SelectedAttraction {
	
	private String attractionName;
	private String animalType;
	private List<Animal> listPerformers = new ArrayList<Animal>();					//upcasting

	private static ArrayList<Attraction> listAtraksi = new ArrayList<Attraction>();	//krn ga mungkin milik pribadi
																					//(ga mungkin 1 doang, jd milik class)

	public AttractionSelection(String jenisHewan, String namaAtraksi) {
		this.animalType = jenisHewan;
		this.attractionName = namaAtraksi;
		listAtraksi.add(this);
	}

    public String getName() {
    	return attractionName;
    }

    public String getType() {
    	return animalType;
    }

    public List<Animal> getPerformers() {
    	return listPerformers;
    }

    public boolean addPerformer(Animal performer) {
    	if(performer.isShowable()) {
    		listPerformers.add(performer);
    		return true;
    	}
    	return false;
    }

    public ArrayList<Attraction> getListAtraksi() {
    	return listAtraksi;
    }

    public Attraction findAttraction(String jenisHewan, String namaAtraksi) {
    	for(Attraction atraksi : listAtraksi) {
    		if(atraksi.getType().equalsIgnoreCase(jenisHewan) && atraksi.getName().equalsIgnoreCase(namaAtraksi)) {
    			return atraksi;
    		}
    	} return null;
    }

    public ArrayList<Attraction> findAttraction(String jenisHewan) {
    	ArrayList<Attraction> atraksiSatuHewan = ArrayList<Attraction>();
    	for(Attraction atraksi : listAtraksi) {
    		if(atraksi.getType().equalsIgnoreCase(jenisHewan)) {
    			atraksiSatuHewan.add(atraksi);
    		}
    	} return atraksiSatuHewan;
    }

	public ArrayList<Attraction> findAttraction(String namaAtraksi) {
    	ArrayList<Attraction> listJenisAtraksi = ArrayList<Attraction>();
    	for(Attraction atraksi : listAtraksi) {
    		if(atraksi.getName().equalsIgnoreCase(namaAtraksi)) {
    			listJenisAtraksi.add(atraksi);
    		}
    	} return listJenisAtraksi;
    }
}