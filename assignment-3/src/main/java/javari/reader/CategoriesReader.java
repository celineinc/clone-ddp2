package javari.reader;

import java.io.IOException;
import java.util.*;

public class CategoriesReader extends CsvReader {
	private String[] validSections = {"Explore the Mammals", "World of Aves", "Reptilian Kingdom"};

	public CategoriesReader(Path fileInput) throws IOException {
		super (fileInput);
	}

	public long countValidRecords() {
		long validCounter = 0;
		for(String line : lines) {
			String[] input = line.split(",");
			String jenisSection = input[2];
			for(String availabeSection : validSections) {
				if(jenisSection.equals(availabeSection)) {
					++validCounter;
				}
			}
		} return validCounter;
	}
	public long countInvalidRecords() {
		long invalidCounter = 0;
		for(String line : lines) {
			String[] input = line.split(",");
			String jenisSection = input[2];
			for(String availabeSection : validSections) {
				if(!jenisSection.equals(availabeSection)) {
					++invalidCounter;
				}
			}
		} return invalidCounter;
	}
}