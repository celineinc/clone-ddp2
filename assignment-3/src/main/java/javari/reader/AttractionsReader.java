package javari.reader;

import java.io.IOException;
import java.util.*;


public class AttractionsReader extends CsvReader {
	private String[] validAttractions = {"Circles of Fires", "Dancing Animals", "Counting Masters", "Passionate Coders"};


	public AttractionsReader(Path fileInput) throws IOException {
		super (fileInput);
	}

	public long countValidRecords() {
		long validCounter = 0;
		for(String line : lines) {
			String[] input = line.split(",");
			String jenisAtraksi = input[1];
			for(String availabeAttractions : validAttractions) {
				if(jenisAtraksi.equals(availabeAttractions)) {
					++validCounter;
				}
			}
		} return validCounter;
	}
	public long countInvalidRecords() {
		long invalidCounter = 0;
		for(String line : lines) {
			String[] input = line.split(",");
			String jenisAtraksi = input[1];
			for(String availabeAttractions : validAttractions) {
				if(!jenisAtraksi.equals(availabeAttractions)) {
					++invalidCounter;
				}
			}
		} return invalidCounter;
	}
}