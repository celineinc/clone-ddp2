package src.main.java;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
* Class in which creating UI and UX
*/
public class FrontEnd {
	
	/** Main Frame */
	protected static JFrame mainFrame;
	
	/** List of cards shown in the game */
	protected static Card[] cards = new Card[36];
	
	/** List of pictures */
	protected ArrayList<ImageIcon> pictures = new ArrayList<ImageIcon>();
	
	/** Number of tries in guessing the card*/
	protected static int numberOfTries;
	
	/** Main Frame */
	protected static JLabel words;
	
	/** Exit Button */
	protected static JButton exitButton;
	
	/** Play Again Button */
	protected static JButton resetButton;
	
	/** Card which is clicked first */
	private static Card clickedFirst;
	
	/**
	* List of BufferedImage
	*/
	private BufferedImage[] pictCollections = {ImageIO.read(new File("img\\Antman.jpg")), ImageIO.read(new File("img\\BlackPanther.jpg")),
											ImageIO.read(new File("img\\BlackWidow.jpg")), ImageIO.read(new File("img\\CaptainAmerica.jpg")),
											ImageIO.read(new File("img\\Falcon.jpg")), ImageIO.read(new File("img\\Hawkeye.jpg")),
											ImageIO.read(new File("img\\Hulk.jpg")), ImageIO.read(new File("img\\IronMan.jpg")), 
											ImageIO.read(new File("img\\Mantis.png")), ImageIO.read(new File("img\\QuickSilver.jpg")), 
											ImageIO.read(new File("img\\ScarletWitch.jpg")), ImageIO.read(new File("img\\Spiderman.jpg")),
											ImageIO.read(new File("img\\Thanos.jpg")), ImageIO.read(new File("img\\Thor.jpg")),
											ImageIO.read(new File("img\\Ultron.jpg")), ImageIO.read(new File("img\\Vision.jpg")),
											ImageIO.read(new File("img\\WarMachine.jpg")), ImageIO.read(new File("img\\Wolverine.jpg"))};
	
	/** Default pictures of the back of the cards*/
	private final ImageIcon AVENGERS = new ImageIcon(ImageIO.read(new File("img\\Avengers.png")));
	
	/**
	* Constructor
	* Initializes frame
	* Create the appearance of the "Match The Avengers" Game
    */
	public FrontEnd() throws IOException{
		mainFrame = new JFrame(); 
		mainFrame.setTitle("Match The Avengers");
		mainFrame.setBackground(Color.BLACK);
		mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		shufflePicts();
		setDefaultPict();
		setCenterPanel();
		setBottomPanel();
		mainFrame.pack();
		mainFrame.setVisible(true);
	}	
	
	/**
    * Method to add pictures that will be played to an array and shuffle them
    */
	private void shufflePicts() {
		for(int i = 0; i < 18; i++) {
			pictures.add(new ImageIcon(pictCollections[i]));
			pictures.add(new ImageIcon(pictCollections[i]));
		} 
		Collections.shuffle(pictures);
	}
	
	/**
    * Method to set the cards into first panel and show the back of the cards first
    */
	private void setDefaultPict() {
		JPanel panelKartu = new JPanel(); 
		panelKartu.setLayout(new GridLayout(6,6));
		panelKartu.setPreferredSize(new Dimension(600, 630));
		for(int i = 0; i < 36; i++) {
			cards[i] = new Card(AVENGERS, pictures.get(i));
			cards[i].addActionListener(new OperationListener());
			cards[i].setIcon(AVENGERS);
			cards[i].setVisible(true);
			panelKartu.add(cards[i]);
		}
		mainFrame.add(panelKartu, BorderLayout.NORTH);
	}
	
	/**
    * Method to set "Exit Button" and "Play Again Button" into second panel
    */	
	private void setCenterPanel() {
		JPanel panelButton = new JPanel();
		panelButton.setPreferredSize(new Dimension(600, 30));
		panelButton.setBackground(Color.BLACK);
		resetButton = new JButton("PLAY AGAIN");
		resetButton.addActionListener(new ButtonListener());
		resetButton.setPreferredSize(new Dimension(110, 25));
		resetButton.setBackground(new Color(192, 192, 192));
		exitButton = new JButton("EXIT");
		exitButton.addActionListener(new ButtonListener());
		exitButton.setPreferredSize(new Dimension(100, 25));
		exitButton.setBackground(new Color(192, 192, 192));
		panelButton.add(resetButton);
		panelButton.add(exitButton);
		mainFrame.add(panelButton, BorderLayout.CENTER);
	}

	/**
    * Method to show the Label of how many attempts have been made in order to win the game
    */
	private void setBottomPanel() {
		JPanel panelBottom = new JPanel();
		panelBottom.setPreferredSize(new Dimension(600, 30));
		panelBottom.setBackground(Color.BLACK);
		words = new JLabel("Number of tries: " + numberOfTries);
		words.setForeground(Color.WHITE);
		panelBottom.add(words);
		mainFrame.add(panelBottom, BorderLayout.SOUTH);
	}
	
	/**
	* Method to check whether all the cards have been matched or not
	* @return boolean, true if the player win the game
    */	
	protected static boolean isWon(){
        for(Card aCard : cards){
            if (aCard.getMatched() == false){
                return false;
            }
        }
        return true;
    }
	
	/**
	* Method to count the number of attempts and show it through the Label
    */	
	protected static void countNumberOfTries() {
        numberOfTries++;
        words.setText("Number of tries: " + numberOfTries);
    }
	
	/**
	* Method to get a card that is clicked first out of two
	* @return Card
    */	
	protected static Card getClickedFirst() {
        return clickedFirst;
    }
   
	/**
	* Method to set a card that is clicked first out of two
	* @param Card	card that is clicked first
    */	
    protected static void setClickedFirst(Card clickedFirst) {
       FrontEnd.clickedFirst = clickedFirst;
    }

	/**
	* Method to reset instance variable "clickedFirst" after 2 cards have been clicked
    */	
    protected static void resetClicked() {
    	FrontEnd.clickedFirst = null;
    }
    
	/*
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent action) {
			if(action.getSource() == exitButton) {
				System.exit(0);
			} else if (action.getSource() == resetButton) {
				mainFrame.dispose();
				new MatchTheAnimal();
				FrontEnd.numberOfTries = 0;
		        FrontEnd.words.setText("Number of tries: " + FrontEnd.numberOfTries);
			}
		}
	}
	
	private class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            cards[currentIndex].setIcon(AVENGERS);
            cards[oddClickIndex].setIcon(AVENGERS);
            timer.stop();
        }
    } */
}