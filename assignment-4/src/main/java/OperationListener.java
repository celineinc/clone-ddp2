package src.main.java;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
* Class that will be called if a Card is clicked
*/
public class OperationListener implements ActionListener {
	private Timer timer;
	
	public void actionPerformed(ActionEvent action) {
		
		/** Temporary store Card that is clicked */
		Card clickedCard = (Card) action.getSource();
		
		/** Show the picture*/
		clickedCard.flip(); 
		
		/** Determine if the card is first clicked or not */
		if (FrontEnd.getClickedFirst() == null) {
            FrontEnd.setClickedFirst(clickedCard);
        } else {
        	/** Determine if the cards is matched or not */
            timer = new Timer(200, new ActionListener() {
                public void actionPerformed(ActionEvent action) {
                	
                    FrontEnd.countNumberOfTries();
                    if((FrontEnd.getClickedFirst().getRealPict().getImage()).equals(clickedCard.getRealPict().getImage()) 
                        && (clickedCard != FrontEnd.getClickedFirst())) {
                    	FrontEnd.getClickedFirst().setEnabled(false);
                        clickedCard.setEnabled(false);
                        FrontEnd.getClickedFirst().setMatched(true);
                        clickedCard.setMatched(true);
                        
                        if (FrontEnd.isWon()) {
                            JOptionPane.showMessageDialog(new JFrame() ,"Game is finished, you win!", "Announcement", JOptionPane.INFORMATION_MESSAGE);
                            System.exit(0);
                        }   
                    } else {
                    	clickedCard.flipBack();
                    	FrontEnd.getClickedFirst().flipBack();
                    }
                    FrontEnd.resetClicked(); 
                }
            });
            
            timer.setRepeats(false);
            timer.start();
        }
	}
}
