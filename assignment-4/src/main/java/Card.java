package src.main.java;

import javax.swing.JButton;
import javax.swing.ImageIcon;

/**
* Class of Card which is formed as Button
*/
public class Card extends JButton {
	
	/** Condition whether the picture has been matched with its pair or not*/
    private boolean isMatched = false;
    
    /** The back of the card (default pictures) */
    private ImageIcon coverPict;
    
    /** The pictures that will be played */
    private ImageIcon realPict;
    
    public Card() {
    	super();
    }
    
    /**
	* Constructor
	* Initializes the back and the front of the card
	* @param ImageIcon, the front and the back of the card
    */
    public Card(ImageIcon coverPict, ImageIcon realPict) {
    	this.coverPict = coverPict;
    	this.realPict = realPict;
    }
    
    /**
	* Method to get the default picture (the back of the card)
	* @return ImageIcon
    */	
    public ImageIcon getCoverPict() {
		return coverPict;
	}
    
    /**
	* Method to get the picture that needs to be matched
	* @return ImageIcon
    */	
	public ImageIcon getRealPict() {
		return realPict;
	}
	
	/**
	* Method to set a card that has been matched with its pair
	* @param booelan,	true if they are pairs
    */	
    public void setMatched(boolean isMatched){
        this.isMatched = isMatched;
    }
    
    /**
	* Method to get a condition whether a card has been matched with its pair or not
	* @return booelan,	true if it has been matched
    */	
    public boolean getMatched(){
        return this.isMatched;
    }
    
    /**
	* Method to flip the card, to show the picture that needs to be matched
    */	
    public void flip() {
        this.setIcon(realPict);
    }
    
    /**
	* Method to flip back the card (default picture)
    */	    
    public void flipBack() {
    	this.setIcon(coverPict);
    }
}