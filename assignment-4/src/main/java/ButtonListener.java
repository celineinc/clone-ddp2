package src.main.java;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;

/**
* Class that will be called if "Exit Button" or "Play Again Button" is pressed
*/
public class ButtonListener implements ActionListener  {
	
	public void actionPerformed (ActionEvent action) {
		
		/** Temporary store Button that is pressed */
		JButton clickedButton = (JButton) action.getSource();
		
		/** Do command due to which button is pressed */
		if(clickedButton == FrontEnd.exitButton) {
			System.exit(0);
		} else if (clickedButton == FrontEnd.resetButton) {
			FrontEnd.mainFrame.dispose();
			try {
				new FrontEnd();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FrontEnd.numberOfTries = 0;
	        FrontEnd.words.setText("Number of tries: " + FrontEnd.numberOfTries);
		}
	}
}
