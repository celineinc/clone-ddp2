import java.util.ArrayList;
import java.util.Scanner;


public class A1Station {
    
    private static final double THRESHOLD = 250; // in kilograms
    private static final double underweight = 18.5;
    private static final double normal = 25;
    private static final double overweight = 30;
    
    
    //MEMBUAT ARRAYLIST UNTUK MENDATA MOBIL YG SUDAH ADA DI TRACK
    private  static ArrayList<TrainCar> track = new ArrayList<TrainCar>();

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        //MEMPROSES KUCING SEBANYAK N
        int jumlahKucing = input.nextInt();
        for (int n = 1; n <= jumlahKucing; n++) {
            //kalo nextLine kok ga bisa, index out of Bound gtu
            String[] dataKucing = input.next().split(",");
            String namaKucing = dataKucing[0];
            double beratKucing = Double.parseDouble(dataKucing[1]);
            double panjangKucing = Double.parseDouble(dataKucing[2]);
            
            //MEMBUAT OBJEK KUCING
            WildCat kucing = new WildCat(namaKucing, beratKucing, panjangKucing);
            
            //If there's no car on the track
            if (track.size() == 0) {
                TrainCar next = new TrainCar(kucing);
                track.add(next);
            } else {
                TrainCar next = new TrainCar(kucing, track.get(track.size() - 1));
                track.add(next);
            }
            if (track.get(track.size() - 1).computeTotalWeight() >= THRESHOLD || (n == jumlahKucing)) {
                depart();
            }
        }
    }
    
    //METHOD UNTUK MEMBERANGKATKAN KERETA
    
    public static void depart() {
            System.out.println("The train departs to Javari Park");
            System.out.print("[LOCO]<");
            track.get(track.size() - 1).printCar();
            //Formatting 2 digit di belakang koma
            String bmi = String.format("%.2f", track.get(track.size() - 1).computeTotalMassIndex() / track.size());
            System.out.println("Average mass index of all cats: " + bmi);
            System.out.println("In average, the cats in the train are " + printAverageBmi());
            track.clear();  //mengosongkan track jika kereta sudah berangkat
    }  
        
    //METHOD UNTUK MENENTUKAN KATEGORI BMI KUCING
    
    public static String printAverageBmi() {
        double averageBmi = track.get(track.size() - 1).computeTotalMassIndex() / track.size();
        if (averageBmi < underweight) {
            return "*underweight*";
        } else if (averageBmi >= underweight && averageBmi < normal) {
            return "*normal*";
        } else if (averageBmi >= normal && averageBmi < overweight) {
            return "*overweight*";
        } else {
            return "*obese*";
        }
    }    
}
