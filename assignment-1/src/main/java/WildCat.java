public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    //KONSTRUKTOR
    
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }
    
    //METHOD UNTUK MENGHITUNG BMI KUCING
    
    public double computeMassIndex() {
        double bmi;
        // krn kalo / 100 itu hasilnya bakal int
        bmi = this.weight / Math.pow(this.length * 0.01, 2);
        return bmi;
    }
}