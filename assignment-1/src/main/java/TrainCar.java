public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    public WildCat cat;
    public TrainCar next;

    public TrainCar(WildCat cat) {
        //konstruktor
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        //konstruktor
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        //Kalkulasi berat mobil dan kucing jika there's no car on the track
        //Base case
        if (this.next == null) {
            return EMPTY_WEIGHT + this.cat.weight;
        
        //Kalkulasi berat mobil dan kucing jika lebih dari 1
        //Recursion
        } else {
            return EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
        }
    }

    public double computeTotalMassIndex() {
        //Kalkulasi BMI 1 kucing
        //Base case
        if (this.next == null) { 
            return this.cat.computeMassIndex();
        
        //Kalkulasi BMI > 1 kucing
        //Recursion
        } else {
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        //Base case utk print jika 1 mobil
        if (this.next == null) {
            System.out.println("--(" + this.cat.name + ")");
            
        //Recursion utk print jika > 1 mobil
        } else {
            System.out.print("--(" + this.cat.name + ")");
            this.next.printCar();
        }
    }
}
